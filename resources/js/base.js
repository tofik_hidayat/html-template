try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');
    /* pluggins */
    /* slick carousel */
    require('slick-carousel');
    
    /*owl carousel */
    require("owl.carousel");

    /* select 2 */
    require("select2");

    /* jquery ui */
    require("jquery-ui-bundle");






} catch (e) {}
