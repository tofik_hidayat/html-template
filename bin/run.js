#! /usr/bin/env node
var shell = require('shelljs');
var cd = './bin/';

// Pass args through to compile script
let args = process.argv.splice(2).join(' ');

try{
	shell.exec('node ' + cd + 'compile.js ' + args);
} catch(e) {
	console.log("oops error");
}